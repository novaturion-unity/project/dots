﻿using Unity.Entities;

namespace SolarDefender.Components
{
	[System.Serializable]
	public struct Cooldown : IComponentData
	{
		[UnityEngine.SerializeField] private float _defaultValue;
		public float value;

		public float DefaultValue { get => _defaultValue; }

		public Cooldown(float defaultValue)
		{
			_defaultValue = this.value = defaultValue;
		}

		public Cooldown(float defaultValue, float value)
		{
			_defaultValue = defaultValue;
			this.value = value;
		}
	}
}