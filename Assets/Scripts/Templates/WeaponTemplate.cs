﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace SolarDefender.Templates
{
	[UnityEngine.CreateAssetMenu(fileName = "NewGun", menuName = "Solar Defender/Entities/Gun", order = 0)]
	public class WeaponTemplate : Template
	{
		public float3 shellSpawnPointValue = default;
		
		protected Translation shellSpawnPoint = default;

		// [UnityEngine.Space]

		// public VisualEffect shotEffect = default;

		// [UnityEngine.Space]

		// public Cooldown cooldown = default;
		// public ShotForce shotForce = default;
		// public Damage damage = default;

		[UnityEngine.Space]

		public ShellTemplate shellValue = default;

		protected override void InitializeComponents() { }
	}
}